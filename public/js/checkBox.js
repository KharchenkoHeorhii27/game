
var countDef = $('#def').text();
var countHit = $('#hit').text();
$('input[type=checkbox].attack').change(function(){
    if($('input.attack[type=checkbox]:checked').length >= countHit){
        $('input.attack[type=checkbox]:not(:checked)').attr('disabled', "disabled");
    } else{
        $('input.attack[type=checkbox]:disabled').removeAttr('disabled');
    }
});


$('input[type=checkbox].defense').change(function(){
    if($('input.defense[type=checkbox]:checked').length >= countDef){
        $('input.defense[type=checkbox]:not(:checked)').attr('disabled', "disabled");
    } else{
        $('input.defense[type=checkbox]:disabled').removeAttr('disabled');
    }
});


