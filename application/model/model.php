<?php

class Model
{
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function getAllPlayer()
    {
        $sql = "SELECT id, name_players, lvl_players, health FROM player";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function addPlayer($name_players)
    {
        $sql = "INSERT INTO player (name_players) VALUES (:name_players)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name_players' => $name_players);
        $query->execute($parameters);
    }

    public function checkExists($name_players)
    {
        $sql = 'SELECT COUNT(*) FROM player WHERE name_players = :name_players';
        $result = $this->db->prepare($sql);
        $result->bindParam(':name_players', $name_players, PDO::PARAM_STR);
        $result->execute();

        return $result->fetchColumn();
    }

    public static function checkNickName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }

        return false;
    }


    public function getPlay($id)        // выбрать одно персонажа
    {
        $sql = "SELECT * FROM player  WHERE player.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function getAllWeapon()          //выбрать всё оружие
    {
        $sql = "SELECT * FROM weapon";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getAllEquipment() //выбрать всю броню
    {
        $sql = "SELECT * FROM equipment";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getWeapon($id)      //выбрать одно оружие
    {
        $sql = "SELECT * FROM weapon WHERE weapon.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function getEquipment($id)   //выбрать одну броню
    {
        $sql = "SELECT * FROM equipment WHERE equipment.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    // Обновление перса / инвентаря
    public function updatePlay($name_players, $lvl_players, $health,
                               $strength, $agility, $personal_damage_players, $personal_armor_players,
                               $id_weapon_rigth, $id_equipment,
                               $id)
    {
        $sql = "UPDATE player SET name_players = :name_players, lvl_players = :lvl_players, health = :health, 
               strength = :strength, agility = :agility,
                                  personal_damage_players = :personal_damage_players, personal_armor_players = :personal_armor_players,
                                  id_weapon_rigth = :id_weapon_rigth, id_equipment = :id_equipment
                                  WHERE player.id = :id";

        $query = $this->db->prepare($sql);

        $parameters = array(':name_players' => $name_players, ':lvl_players' => $lvl_players, ':health' => $health,
            ':strength' => $strength, ':agility' => $agility,
            ':personal_damage_players' => $personal_damage_players, ':personal_armor_players' => $personal_armor_players,
            ':id_weapon_rigth' => $id_weapon_rigth, ':id_equipment' => $id_equipment,
            ':id' => $id);
        $query->execute($parameters);
    }


    /*    public function getAmountOfSongs()
        {
            $sql = "SELECT COUNT(id) AS amount_of_songs FROM song";
            $query = $this->db->prepare($sql);
            $query->execute();

            // fetch() is the PDO method that get exactly one result
            return $query->fetch()->amount_of_songs;
        }*/
}
