<?php

class BattleModel extends Model
{
    public function getPlay($id)        // выбрать одного персонажа
    {
        $sql = "SELECT * FROM player  WHERE player.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function getWeapon($id)      //выбрать одно оружие
    {
        $sql = "SELECT * FROM weapon WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function getEquipment($id)   //выбрать одну броню
    {
        $sql = "SELECT * FROM equipment WHERE equipment.id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function setCurrentHealth($id, $currentHealth = '' )
    {
        if (!empty($currentHealth)){
            $sql = "UPDATE player SET current_health = '" . (int) $currentHealth . "' WHERE id = " . (int) $id ;

        } else {
            $sql = "UPDATE player SET current_health = health WHERE id = " . (int) $id ;
        }

        $query = $this->db->prepare($sql);
        $parameters = array(':current_health' => $currentHealth, ':id' => $id);
        $query->execute($parameters);

    }

    public function getCurrentHealth($id)
    {
        $sql = "SELECT current_health FROM player WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function setCurrentHealthBot($id, $currentHealth = '' )
    {
        if (!empty($currentHealth)){
            $sql = "UPDATE player SET current_health_bot = '" . (int) $currentHealth . "' WHERE id = " . (int) $id ;

        } else {
            $sql = "UPDATE player SET current_health_bot = health WHERE id = " . (int) $id ;
        }

        $query = $this->db->prepare($sql);
        $parameters = array(':current_health' => $currentHealth, ':id' => $id);
        $query->execute($parameters);
    }

    public function getCurrentHealthBot($id)
    {
        $sql = "SELECT current_health_bot FROM player WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);

        return $query->fetch();
    }

    public function addBattle($log_round = '')
    {
        $sql = "INSERT INTO battle (log_round) VALUES (:log_round)";
        $query = $this->db->prepare($sql);
        $parameters = array(':log_round' => $log_round);
        $query->execute($parameters);
    }


}