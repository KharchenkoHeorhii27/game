<?php

class Log extends Model
{

    public function getLog()
    {
        $sql = "SELECT battle_log.id,result.result_battle,player.name_players,battle_log.log
				FROM battle_log INNER JOIN player ON battle_log.id_players = player.id
					     		INNER JOIN result ON battle_log.id_result = result.id
                                ORDER BY id DESC";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lastLog()
    {
        $sql = "SELECT battle_log.id,result.result_battle,player.name_players,battle_log.log FROM battle_log 
                  INNER JOIN player ON battle_log.id_players = player.id
				  INNER JOIN result ON battle_log.id_result = result.id
                  ORDER BY id DESC LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function raundLog()
    {
        $sql = "SELECT * FROM battle ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();

    }
}