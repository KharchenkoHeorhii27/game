<?php


class Personage extends Controller
{
    private $weaponHit;
    private $equipBoost;
    private $fir;
    private $getCurrentHealth;
    private $getCurrentHealthBot;
    private $setCurrentHealth;
    private $setCurrentHealthBot;
    private $sumDamagPersonage;
    private $sumDamagBot;

    function __construct()
    {
        parent::__construct();

        $this->fir = $this->model->getPlay((int)$_SESSION['id_player']);
        $this->weaponHit = $this->model->getWeapon($this->fir->id_weapon_rigth);
        $this->equipBoost = $this->model->getEquipment($this->fir->id_equipment);

        $this->damagePersonage();
        $this->damageBot();
        $this->countHelthPersonage();
        $this->countHelthBot();

        $this->calculateDamage($this->weaponHit->count_attack, $this->weaponHit->count_def);

        $this->getCurrentHealth = $this->battle->getCurrentHealth($this->fir->id)->current_health - $this->sumDamagBot;
        $this->getCurrentHealthBot = $this->battle->getCurrentHealthBot($this->fir->id)->current_health_bot - $this->sumDamagPersonage;

        $this->setCurrentHealth = $this->battle->setCurrentHealth($this->fir->id, $this->getCurrentHealth);
        $this->setCurrentHealthBot = $this->battle->setCurrentHealthBot($this->fir->id, $this->getCurrentHealthBot);

        //$this->checkCurrentHP($this->getCurrentHealth,$this->getCurrentHealthBot);

        $test = "Персонаж  " . $this->fir->name_players . " Текущ хп персонажа  " . $this->fir->current_health . " Урон нанес персонаж  " .
            $this->sumDamagPersonage . " Текущ хп бот " . $this->fir->current_health_bot . " Урон нанес бот   " .
            $this->sumDamagBot . " RESULT = " . $this->checkCurrentHP($this->getCurrentHealth, $this->getCurrentHealthBot);

        $this->modelBattle = $this->battle->addBattle($test);

        //header('location: ' . URL . '/battle/');
        var_dump($this->getCurrentHealth);
        var_dump($this->getCurrentHealthBot);
    }

    public function index()
    {

    }

    private function checkCurrentHP($healthPers, $healthBot)
    {
        $some = '';
        if ($healthPers <= 0) {
            $some = " Your lose AHAHAHAHAH";
            return $some;
        }
        if ($healthBot <= 0) {
            $some = " BOT LOSE YOU WIN GG IZI KATKA";
            return $some;
        }

    }

    private function countHelthPersonage()
    {
        return round($this->fir->health + $this->equipBoost->hp_boost + ($this->fir->strength * 1.5) + ($this->fir->agility * 2.5));
    }

    private function countHelthBot()
    {
        $he = round(($this->fir->health + $this->equipBoost->hp_boost + ($this->fir->strength * 1.5) + ($this->fir->agility * 2.5)) * 0.9);
        return $he;
    }

    private function damagePersonage()
    {
        $dam = round($this->weaponHit->damage_weapon + $this->fir->personal_damage_players + ($this->fir->strength * 2) + ($this->fir->agility * 1.8));
        return $dam;
    }

    private function damageBot()
    {
        $dam = round(($this->weaponHit->damage_weapon + $this->fir->personal_damage_players + ($this->fir->strength * 2) + ($this->fir->agility * 1.8)) * 0.95);
        return $dam;
    }

    private function calculateDamage($hit, $def)
    {
        $valuecheckAttack = [];
        $valuecheckDefense = [];

        for ($i = 1; $i <= 5; $i++) {
            if (isset($_POST['mygroup' . $i])) {
                array_push($valuecheckAttack, $i);
            }
        }

        for ($i = 6; $i <= 10; $i++) {
            if (isset($_POST['mygroup' . $i])) {
                $k = $i - 5;
                array_push($valuecheckDefense, $k);
            }
        }

        $this->sumDamagBot = ($this->checkAttack($this->actionBot($hit), $valuecheckDefense) * $this->damageBot());
        $this->sumDamagPersonage = ($this->checkAttack($valuecheckAttack, $this->actionBot($def)) * $this->damagePersonage());
        echo " Вы нанесли урон = " . $this->sumDamagPersonage;
        echo " Бот нанес = " . $this->sumDamagBot;
    }

    private function actionBot($count = 2)
    {
        $outArray = array();
        $i = 0;
        while ($i < $count) {
            $value = mt_rand(1, 5);
            if (!in_array($value, $outArray)) {
                $outArray[$i] = $value;
                $i++;
            }
        }

        return $outArray;
    }

    private function checkAttack(array $attack, array $defence)
    {
        $sum = 0;
        foreach ($attack as $value) {
            if (!in_array($value, $defence)) {
                $sum += 1;
            }
        }

        return $sum;
    }
}