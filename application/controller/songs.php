<?php


class Songs extends Controller
{
    public function index()
    {
        // $amount_of_songs = $this->model->getAmountOfSongs();
        $player = $this->model->getAllPlayer();

        require APP . 'view/_templates/header.php';
        require APP . 'view/songs/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function addPlayer()
    {
        if (isset($_POST['submit_add_player'])) {
            $name_players = $_POST['name_players'];

            $errors = false;

            if ($this->model->checkExists($name_players)) {
                $errors[] = 'такой персонаж сущ';
            }
            if (!Model::checkNickName($name_players)) {
                $errors[] = 'ник больше 2 символов';
            }
            if (!$errors) {
                // do addSong() in model/model.php
                $this->model->addPlayer($_POST["name_players"]);
            }
        }

        header('location: ' . URL . 'songs/index');

        return true;
    }

    public function editPlay($id)
    {
        if (isset($id)) {
            $bat_id = 0;
            $players = $this->model->getPlay($id);
            $weapon = $this->model->getAllWeapon();
            $equipment = $this->model->getAllEquipment();
            $weaponOne = $this->model->getWeapon($players->id_weapon_rigth);
            $armorOne = $this->model->getEquipment($players->id_equipment);
            $_SESSION['id_player'] = $id;
            require APP . 'view/_templates/header.php';
            require APP . 'view/songs/play.php';
            require APP . 'view/_templates/footer.php';
        } else {
            header('location: ' . URL . 'songs/index');
        }
    }

    public function updatePlay()
    {
        if (isset($_POST["submit_update_play"])) {
            $this->model->updatePlay($_POST["name_players"], $_POST["lvl_players"], $_POST["health"],
                $_POST["strength"], $_POST["agility"], $_POST["personal_damage_players"], $_POST["personal_armor_players"],
                $_POST["id_weapon_rigth"], $_POST["id_equipment"],
                $_POST['id']);
        }
        header('location: ' . URL . 'songs/editplay/'. $_POST['id']);
    }


    /*public function ajaxGetStats()
    {
        $amount_of_songs = $this->model->getAmountOfSongs();
        // simply echo out something. A supersimple API would be possible by echoing JSON here
        echo $amount_of_songs;


        public function deleteSong($song_id)
    {
        // if we have an id of a song that should be deleted
        if (isset($song_id)) {
            // do deleteSong() in model/model.php
            $this->model->deleteSong($song_id);
        }

        // where to go after song has been deleted
        header('location: ' . URL . 'songs/index');
    }
    }*/
}