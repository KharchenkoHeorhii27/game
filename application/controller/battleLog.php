<?php

/**
 * Created by PhpStorm.
 * User: PC-Devs
 * Date: 27.06.2017
 * Time: 12:41
 */
class BattleLog extends Controller
{
    public function index()
    {
        $log = $this->log->getLog();
        require APP . 'view/_templates/header.php';
        require APP . 'view/battlelog/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function lastLog()
    {
        $log = $this->log->lastLog();
        $raundLog = $this->log->raundLog();

        require APP . 'view/_templates/header.php';
        require APP . 'view/battlelog/last.php';
        require APP . 'view/_templates/footer.php';
    }
}