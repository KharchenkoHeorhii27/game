<div class="container">

    <div class="box">
        <h3>Add a name_players</h3>
        <form action="<?php echo URL; ?>songs/addPlayer" method="POST">
            <label>Artist</label>
            <input type="text" name="name_players" value="" required />

            <input type="submit" name="submit_add_player" value="Submit" />
        </form>
    </div>

    <!-- main content output -->
    <div class="box">
        <h3>Amount of songs (data from second model)</h3>
        <div>
            <?php echo $amount_of_songs; ?>
        </div>
        <h3>Amount of songs (via AJAX)</h3>
        <div>
            <button id="javascript-ajax-button">Click here to get the amount of songs via Ajax (will be displayed in #javascript-ajax-result-box)</button>
            <div id="javascript-ajax-result-box"></div>
        </div>
        <h3>List of songs (data from first model)</h3>


        <table>
            <thead style="background-color: #ddd; font-weight: bold;">
            <tr>
                <td>Id</td>
                <td>NickName</td>
                <td>LVL</td>
                <td>Health</td>
                <td>Go</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($player as $players) : ?>
                <tr>

                    <td><?php echo htmlspecialchars($players->id, ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo htmlspecialchars($players->name_players, ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo htmlspecialchars($players->lvl_players, ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo htmlspecialchars($players->health, ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><a href="<?php echo URL . 'songs/editplay/' . htmlspecialchars($players->id, ENT_QUOTES, 'UTF-8'); ?>">PLAY</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
