<div class="container">
    <div>
        <h3>Edit a player</h3>
        <form action="<?php echo URL; ?>songs/updateplay" method="POST">
            <label>name_players</label>
            <input autofocus type="text" name="name_players"
                   value="<?php echo htmlspecialchars($players->name_players, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>lvl_players</label>
            <input type="text" name="lvl_players"
                   value="<?php echo htmlspecialchars($players->lvl_players, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>health</label>
            <input type="text" name="health"
                   value="<?php echo htmlspecialchars($players->health, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>strength</label>
            <input type="text" name="strength"
                   value="<?php echo htmlspecialchars($players->strength, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>agility</label>
            <input type="text" name="agility"
                   value="<?php echo htmlspecialchars($players->agility, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>personal_damage_players</label>
            <input type="text" name="personal_damage_players"
                   value="<?php echo htmlspecialchars($players->personal_damage_players, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>personal_armor_players</label>
            <input type="text" name="personal_armor_players"
                   value="<?php echo htmlspecialchars($players->personal_armor_players, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <label>avatar</label>
            <input type="text" name="avatar"
                   value="<?php echo htmlspecialchars($players->avatar, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>

            <label>id_weapon_rigth</label>
            <select name="id_weapon_rigth">
                <optgroup label="Выбор оружия">
                    <?php foreach ($weapon as $item) : ?>
                        <option value="<? echo $item->id ?>"><?php echo $item->name_weapon . " Урон = " . $item->damage_weapon . " Кол ударов = " . $item->count_attack . " Кол блокировок = " . $item->count_def ?></option>
                    <?php endforeach; ?>
                </optgroup>
            </select>
            <br>

            <label>id_equipment</label>
            <select name="id_equipment">
                <optgroup label="Выбор брони">
                    <?php foreach ($equipment as $armor) : ?>
                        <option value="<? echo $armor->id ?>"><?php echo $armor->name_armor . " Броня = " . $armor->armor_boost_count . " Буст Хп = " . $armor->hp_boost ?></option>
                    <?php endforeach; ?>
                </optgroup>
            </select>
            <br>

            <?php
            $weap = $weaponOne->damage_weapon;
            $arm = $armorOne->armor_boost_count;
            $armHealth = $armorOne->hp_boost;


            $totalDamag = round(($weap + $players->personal_damage_players) + $players->strength);
            $totalArmor = round($arm + $players->personal_armor_players + $players->agility);
            $totalHealth = round($armHealth + $players->health + $players->strength / 2 + $players->agility / 1.5);
            ?>

            <br>
            <label><?php echo 'Общий урон = ' . $totalDamag ?></label>
            <br>
            <label><?php echo 'Общая броня = ' . $totalArmor ?></label>
            <br>
            <label><?php echo 'Кол хп = ' . $totalHealth ?></label>
            <br>

            <input type="hidden" name="id" value="<?php echo htmlspecialchars($players->id, ENT_QUOTES, 'UTF-8'); ?>"/>
            <br>
            <input type="submit" name="submit_update_play" value="Update"/>
        </form>
    </div>

    <?php include_once "change_dif.php" ?>

</div>

