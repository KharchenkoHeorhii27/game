<div class="container">
    <table>
        <thead style="background-color: #ddd; font-weight: bold;">
        <tr>
            <td>Id</td>
            <td>id_players</td>
            <td>result</td>
            <td>log</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($log as $itemLog) { ?>
            <tr>
                <td><?php echo htmlspecialchars($itemLog->id, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php echo htmlspecialchars($itemLog->name_players, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php echo htmlspecialchars($itemLog->result_battle, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php echo htmlspecialchars($itemLog->log, ENT_QUOTES, 'UTF-8'); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <table>
        <thead style="background-color: #ddd; font-weight: bold;">
        <tr>
            <td>Id</td>
            <td>raundLog</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($raundLog as $itemLog) : ?>
            <tr>
                <td><?php echo htmlspecialchars($itemLog->id, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php echo htmlspecialchars($itemLog->log_round, ENT_QUOTES, 'UTF-8'); ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>