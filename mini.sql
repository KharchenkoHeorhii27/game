-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 30 2017 г., 09:33
-- Версия сервера: 5.5.48
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mini`
--

-- --------------------------------------------------------

--
-- Структура таблицы `all_battle`
--

CREATE TABLE IF NOT EXISTS `all_battle` (
  `id` int(11) NOT NULL,
  `id_battle_log` int(11) NOT NULL,
  `id_battle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `battle`
--

CREATE TABLE IF NOT EXISTS `battle` (
  `id` int(11) NOT NULL,
  `count_round` int(11) unsigned NOT NULL,
  `log_round` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `battle_log`
--

CREATE TABLE IF NOT EXISTS `battle_log` (
  `id` int(11) NOT NULL,
  `id_players` int(11) NOT NULL,
  `id_result` int(11) NOT NULL,
  `log` text NOT NULL,
  `id_all` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `battle_log`
--

INSERT INTO `battle_log` (`id`, `id_players`, `id_result`, `log`, `id_all`) VALUES
(1, 1, 1, 'Xnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnjXnj xnj', 0),
(2, 2, 2, 'dasdasdasda', 0),
(3, 2, 1, 'ffffffffffffffff', 0),
(4, 44, 1, 'fffffffffds333333333333ffffffff', 0),
(5, 51, 1, 'ffffffffffffffff', 0),
(6, 43, 1, 'fjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(11) NOT NULL,
  `name_armor` varchar(100) NOT NULL,
  `armor_boost_count` int(10) unsigned NOT NULL,
  `hp_boost` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `equipment`
--

INSERT INTO `equipment` (`id`, `name_armor`, `armor_boost_count`, `hp_boost`) VALUES
(1, 'Обычная одежда', 2, 10),
(2, 'Броня Волка', 20, 100),
(3, 'Броня Дракона', 40, 200);

-- --------------------------------------------------------

--
-- Структура таблицы `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL,
  `name_players` varchar(50) NOT NULL,
  `lvl_players` int(10) unsigned NOT NULL DEFAULT '1',
  `avatar` varchar(70) NOT NULL DEFAULT '/что-то/сюда/добавить',
  `id_users` int(11) NOT NULL DEFAULT '1',
  `id_weapon_rigth` int(11) NOT NULL DEFAULT '1',
  `id_weapon_left` int(11) NOT NULL DEFAULT '2',
  `id_equipment` int(11) NOT NULL DEFAULT '1',
  `strength` int(10) unsigned NOT NULL DEFAULT '10',
  `agility` int(10) unsigned NOT NULL DEFAULT '10',
  `health` int(10) unsigned NOT NULL DEFAULT '100',
  `personal_damage_players` int(11) unsigned NOT NULL DEFAULT '3',
  `personal_armor_players` int(11) unsigned NOT NULL DEFAULT '5',
  `current_health` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `player`
--

INSERT INTO `player` (`id`, `name_players`, `lvl_players`, `avatar`, `id_users`, `id_weapon_rigth`, `id_weapon_left`, `id_equipment`, `strength`, `agility`, `health`, `personal_damage_players`, `personal_armor_players`, `current_health`) VALUES
(1, 'GodLike', 8, 'avatarka', 1, 1, 3, 1, 25, 20, 505, 4, 5, 0),
(2, 'N0rm', 22, 'avatarka', 1, 2, 2, 2, 10, 10, 100, 3, 5, 0),
(41, 'Kot', 15, '/что-то/сюда/добавить', 1, 2, 2, 2, 20, 10, 760, 3, 5, 0),
(42, 'test666', 4, '/что-то/сюда/добавить', 1, 2, 2, 1, 20, 50, 500, 30, 52, 0),
(43, 'Shift', 5, '/что-то/сюда/добавить', 1, 1, 2, 1, 20, 15, 100, 3, 5, 0),
(44, 'пап', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(45, 'fzfzf', 1, '/что-то/сюда/добавить', 1, 3, 2, 1, 10, 10, 100, 3, 5, 0),
(46, 'agag', 1, '/что-то/сюда/добавить', 1, 1, 2, 2, 10, 10, 100, 3, 5, 0),
(47, 'ho', 1, '/что-то/сюда/добавить', 1, 3, 2, 3, 10, 10, 100, 3, 5, 0),
(48, '77', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(49, 'fgg', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(50, 'miss', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(51, '767', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(52, 'IDDQD', 12, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(53, 'WhosYourDaddy?', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(58, '12345', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0),
(59, '123123', 1, '/что-то/сюда/добавить', 1, 1, 2, 1, 10, 10, 100, 3, 5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) unsigned NOT NULL,
  `result_battle` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `result`
--

INSERT INTO `result` (`id`, `result_battle`) VALUES
(1, 'Победа'),
(2, 'Поражение');

-- --------------------------------------------------------

--
-- Структура таблицы `weapon`
--

CREATE TABLE IF NOT EXISTS `weapon` (
  `id` int(11) NOT NULL,
  `name_weapon` varchar(100) NOT NULL,
  `damage_weapon` int(10) unsigned NOT NULL,
  `critical_chance_weapon` int(11) unsigned NOT NULL,
  `power_critical_weapon` int(11) unsigned NOT NULL,
  `count_attack` int(11) unsigned NOT NULL,
  `count_def` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `weapon`
--

INSERT INTO `weapon` (`id`, `name_weapon`, `damage_weapon`, `critical_chance_weapon`, `power_critical_weapon`, `count_attack`, `count_def`) VALUES
(1, 'Старый нож', 5, 10, 2, 2, 1),
(2, 'Старый щит', 4, 0, 0, 1, 3),
(3, 'КЛИНОК ПАДШЕГО ПРИНЦА', 50, 5, 10, 4, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `all_battle`
--
ALTER TABLE `all_battle`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `battle`
--
ALTER TABLE `battle`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `battle_log`
--
ALTER TABLE `battle_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_armor` (`name_armor`);

--
-- Индексы таблицы `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_players` (`name_players`),
  ADD KEY `player_equipment_id_fk` (`id_equipment`);

--
-- Индексы таблицы `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `weapon`
--
ALTER TABLE `weapon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_weapon` (`name_weapon`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `all_battle`
--
ALTER TABLE `all_battle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `battle`
--
ALTER TABLE `battle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `battle_log`
--
ALTER TABLE `battle_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT для таблицы `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `weapon`
--
ALTER TABLE `weapon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
